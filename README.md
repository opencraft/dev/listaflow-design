# Listaflow-design

This repository keeps track of publicly accessible copies of Listaflow design files. We export them here to avoid cluttering the main repository.

### Development

To view information about the development of this project, visit the [main repository](https://gitlab.com/opencraft/dev/listaflow).
