# Listaflow - UX Discovery

## Introduction

Now that the [proposal for Listaflow][listaflow-proposition-requirements-and-initial-steps] has been defined and agreed upon, it’s time to plan the UX and UI portion of the project. The goal of this discovery is to scope the UX/UI tasks, and estimate, as accurately as possible, the monthly UX/UI work that will be required. 

## Project Approach

The general rule of thumb for this project is to design and implement small portions of each feature at a time, and to use proof of concepts, and prototypes wherever possible.

Since the UX/UI team and  managerthe development team will be working closely together, the UX tasks have been grouped into stages with the intention of giving each team manageable chunks of work to tackle during each sprint. The goal is for the UX/UI and development tasks to run concurrently - while developers work on the outcome of the first UX task, the second UX task progresses in preparation for the next developer sprint, and so on. 

Together with Xavier Antoviaque, Developers will be the reviewers on UX/UI tasks. In turn, Fixate (Ali/Cassie) will be the 2nd reviewers on development tasks. This way, developers can provide feedback on what can be achieved during their next sprint. From there, Fixate will write stories for the developers during sprint planning, and the epic owner will refine and/or split them to take technical constraints into account.

## Usability Testing

A number of sprints will be dedicated to usability testing. The goal of usability testing is to get feedback from real-world users (whether they be OpenCraft team members, or external users) while the application is being developed. This feedback will inform the direction taken in future sprints. 

For each usability test, we select a portion of the application that has been developed (this can be in the form of a prototype, or a fully-fledged feature). We then test the feature with a handful of participants (a sample size of 5 participants is usually sufficient). Each test is video-based and includes a number of tasks and/or questions. For remote tests, participants are encouraged to record themselves and their screen (using a service like [Loom][loom-com]), and voice their thoughts as they interact with the feature. For live tests, the interviewer will be responsible for the recording.

Once all the usability tests have been conducted, Fixate will evaluate the videos and put together a list of the main points that were mentioned. From there, we’ll adjust the design files accordingly. Depending on the feedback provided, future sprints may need to be adjusted to account for the scope of the proposed solution.

## Features 

Below is a preliminary list of the main features we expect will form part of Listaflow. They are listed roughly in the intended order of implementation; however, this order may change - and in fact, so may the choice of features. This is because we are taking an iterative approach in which the goals of the project are constantly adjusted based on feedback from usability testing, new ideas, unexpected realisations etc.

### 1. Preliminary Usability Testing
 
We’ll kickstart the project with a round of usability tests. This will help to validate some of the ideas we have in mind for Listaflow. It will also help us decide on the best way to approach the first feature we implement (a [vote][forum-vote] on the forum decided the [Vacation Checklist][vacation-checklist] should be built first). 
 
Usability testing will be conducted with at least 5 participants. This may include members of the OpenCraft team, or external users. Participants will be provided with a list of tasks to complete, as well as a number of questions to answer.

### 2. Workflow: Table View

This is a view of a workflow that showcases list(s) of tasks and their attributes in a table layout. The tasks may be divided into task groups. The eventual goal for the table view may possibly be that it is similar to that of [monday.com][monday-com]. 

- Workflow
- Task Group
- Task
  - title
  - priority
  - status
  - start date
  - end date
  - external link (possibly to a thread on the forum)
- Sort table by column
- **Note**: no user/group/role assignment will be implemented yet

---

### 3. Workflow: Documentation View

This is a view of a workflow with its list(s) of tasks, their attributes, as well as accompanying documentation. The documentation view will be similar in layout to [Process Street’s checklists][process-street-checklist].

- Workflow
- Task Group 
- Task 
- Documentation
  - text
  - image
  - video
  - file
- **Note**: no user/group/role assignment will be implemented yet

---

### 4. Subtasks and Task Dependencies

Allow users to add subtasks to existing tasks.

Also provide the ability to create relationships between different tasks where the dates of the two tasks are connected i.e if the start or end date of the main task changes, the dates of the dependent task will adjust automatically.

---

### 5. User Access / Management

It should be quick and easy for users to sign up, log in, adjust their profile details, and invite other users to the application.

- User registration
  - name
  - email
- User authentication
  - email 
  - password
- Forgotten password
- Password reset
- Invite users to the application
  - invite with link
  - invite with email
- Profile management
  - name
  - email
  - title/role
  - timezone
  - delete account

---

### 6. Comments

Add commenting to workflows, and tasks, allowing users the opportunity to discuss any unexpected updates, or to notify other users when something needs to be brought to their attention.

- Commenting:
  - workflows
  - tasks
  
---

### 7. Workflow Runs

A workflow run is an instance of a specific workflow. It has a specific start and end date, as well as assigned user(s).

- Workflow Run
  - title
  - start date
  - end date
  - assignee(s)
  - commenting

---

### 8. Search and Filter Workflows, Workflow Runs, and Tasks

Provide users with an easy way to find specific workflows, workflow runs, or tasks through filtering or searching.

- Filter by:
  - user
  - priority
  - status
  - start date
  - end date
- Search by:
  - title 
  - user
  - priority
  - status
  - start date
  - end date

---

### 9. Progress tracking tools

With progress tracking tools, we can clearly display the progress of a specific workflow run. Through the use of charts, we can represent progress in an easy-to-digest manner, giving everyone a clear idea of the team’s progress. Users will be able to see what's been done, what's left to do, and what communication might be required.


- Filter charts by:
  - workflow
  - workflow run
  - user(s)

---

### 10. User Groups and Conditional Tasks

Allow users to be grouped according to their role, or any other distinguishing factor. From there, the concept of “conditional tasks” can be introduced.

A conditional task is a task that is specific to a certain user group. With conditional tasks, the same workflow can be used for different user groups i.e. although a *workflow* may contain tasks for all user groups, the *workflow run* would only display the information relevant to the specific user.

- User Group
  - title
  - users
- Filter and search by User Group
- Conditional Tasks
  - Assign tasks to:
    - user(s)
    - user group(s)
  - Assign tasks from:
    - table view
    - documentation view

---

### 11. Emails and Reminders

Provide an easy way to send follow-up emails or reminders to users and/or user groups to bring their attention to upcoming items, or tasks that are behind schedule and may have been missed.

---

### 12. Organisations

Allow users to add an organisation, and to invite other users to join it. An organisation contains its own workflows, workflow runs, tasks, users etc. Organisations may choose whether to make their workflows public or not.

Should a user be associated with more than one organisation, provide them with an easy way to switch between the different organisations.

- Organisation
  - title
  - users
  - workflows
    - public
    - private
- Invite users to an organisation
  - invite with link
  - invite with email

---

### 13. Define Billing Plans
 
Determine what options to offer users in terms of billing, as well as how billing options will be structured. 

---

### 14. Dedicated Marketing Site

Build a marketing site that describes the application, outlines its features, and explains its pricing plans. Interested users will be encouraged, either to create an account, or to view the documentation should they wish to install the application themselves. 

---

### 15. Automation Tools
Add automation tools to offer users the ability to automate certain tasks from their checklists. 

---

### 16. Developer Tools

Allow third party software to manage workflows and workflow runs, and to obtain information regarding the progress of a task or workflow run.

---

### 17. Product Tour
Add a product tour to help users get their bearings. The tour will walk users through the interface, introducing them to the application’s features, and telling them how to use them.

---

### 18. Automate Billing 

Update billing from a manual process to an automated process. 

- Billing schedule
  - monthly
  - annual
- User billing profile
  - billing preferences
  - payment history
  - cancel plan
  
---

## Prioritised Feature List

In accordance with the [Project Approach][project-approach] outlined above, sprints should be planned with the following points in mind:

- Development is scheduled based on the UX/UI designs 
- Changes are tested and UX reviewed before any merge
- Development iterations are small (1 release per sprint, from the very first sprint)
- Key features will undergo usability testing (with internal, and external users)
- Each set of usability testing will likely span 2 sprints, as it includes preparing the tests, conducting them, evaluating the feedback, and adjusting design files accordingly

Below are the features we expect Listaflow to include (these features may change). They are listed in the proposed order of implementation, however, this order will likely adjust as the project progresses. UX sprints will remain flexible, and can adjust according to how development sprints have been structured.

 1. ` Preliminary Usability Testing `
 1. Workflow: Table View
 1. Workflow: Documentation View
 1. Subtasks and Task Dependencies
 1. User Access / Management
 1. Comments
 1. ` Usability Testing `
 1. Workflow Runs
 1. Search and Filter Workflows, Workflow Runs, and Tasks
 1. Progress Tracking Tools
 1. User Groups and Conditional Tasks
 1. ` Usability Testing `
 1. Emails and Reminders
 1. Organisations
 1. Defining Billing Plans
 1. Dedicated Marketing Site
 1. ` Usability Testing `
 1. Automation Tools
 1. Developer Tools
 1. ` Usability Testing `
 1. Product Tour
 1. Automate Billing
 1. ` Usability Testing `

## Monthly UX Time Commitment

We estimate a total of **120 - 180 hours** per month will be required for the UX portion of this project. The first UX sprint can be SE:242 (starting 9 March).

**Please note:**

- Usability testing will incur additional 3rd party costs if we are required to recruit external users to test the application.
- This estimated number of UX hours that can be dedicated to this project monthly does not take into account:
  - UX hours that may potentially be required on other OpenCraft projects (such as Open edX Theming, OpenCraft Theme, Sprints v2, and Billing v2). If suitable, allocation of UX hours can remain flexible depending on the priority of the projects. 
  - Leave time

## Project Roles

- Product management: Fixate (Ali/Cassie), reviewed by Xavier
- Reviewers on UX/UI tasks: Xavier Antoviaque and Developers
- 2nd reviewers on development tasks: Fixate (Ali/Cassie)

<!-- LINKS -->

[listaflow-proposition-requirements-and-initial-steps]: https://gitlab.com/opencraft/dev/listaflow/-/blob/master/docs/discoveries/workflow-manager-proposition-requirements-and-initial-steps.md
[loom-com]: https://www.loom.com/
[forum-vote]: https://forum.opencraft.com/t/workflow-manager-what-to-build-first/844
[vacation-checklist]: https://handbook.opencraft.com/en/latest/vacation/#vacation-checklist
[monday-com]: https://view.monday.com/942397140-b2d0d779a236cf486837f8c7391a08d9
[process-street-checklist]: https://www.process.st/checklist/client-onboarding-for-a-marketing-agency/
[project-approach]: #project-approach
