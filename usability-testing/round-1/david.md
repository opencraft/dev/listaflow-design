# Quick links

- [User Interview Spreadsheet Notes](https://docs.google.com/spreadsheets/d/19L6V47JrMj42XaePnE-7PfVjodKkdkrMmoPjd_ozVGM/edit?usp=sharing)
- [User Interview Video](https://drive.google.com/drive/u/2/folders/12x1uUj5NaqhqnN0KMA_YfMD8HCi_yuRk)

We've included a summary of the User Interview below.

# User Interview Summary

**1. Can you tell me a bit about your current role at your organisation?**

David is the cofounder of Studio XP.

Studio XP is a video-game school, where kids are taught how to create video-games.

David is in charge of the operations and organization, such as the team of teachers and the team that develops tools and course content.

**2. I imagine there’s a fair amount of organisation that goes into managing the various people and processes at your company. How do you keep on top of this?**

The organization of teachers' and developers' work is different. It creates different "cultures", according to David, which makes the work both interesting and difficult.

Slack is used for the communication between all members, teachers and developers. Meanwhile, Monday is used mainly for the core team of developers, excluding the teachers.

Custom spreadsheets are used for the planning and organization of the teacher's work.

**3. Does your team use checklists to manage their workflows?**

N/A

- If so, do you have any frustrations related to how checklists are currently being used at your organisation?

  N/A

**4. Do you use any tools for task management or issue tracking? If so:**

The development team at Studio XP uses Monday for SCRUM user stories.

Custom spreadsheets are used for the planning and organization of the teacher's work.

- Is there anything about the tool that you particularly like? (feel free to share your screen if you’d like to highlight specific aspects of the tool)

  The team likes the UX in Monday and finds it helpful, because it's easy to use for non-tech-savvy people.

  He mentions that he is not a fan of the way they display ____ (connection made it hard to pick up what exactly he is not a fan of).

- Do you dislike anything about the tool?

  The are three issues:
  1. Not everyone uses Monday. Studio XP would love to include teachers in Monday for different reasons such as requesting new features for them and have them vote on the features they'd like transformed into user stories to be applied. Unfortunately, they are not able today to integrate the core team of developers that use Monday with the teams that do not, such as the teachers. It creates a wall for them (the additional clarification as to why they are not able to integrate the teachers with the developers is a little bit unclear due to the connection issues).
  1. The teams' workflows are significantly different.  The development team's workflow is SCRUM based. Meanwhile the teachers rely significantly on planning, schedules and deadlines. The team hasn't yet found a way to manage all the different workflows, simultaneously, within Monday.
  1. The internal wiki is not integrated with Monday.  The workflow on Monday doesn't keep track of the tickets that still require documentation to be done by the teachers. So the ticket might be stuck on the documentation process; however, that information would not be reflected on Monday (additional clarification was hard to grasp due to the poor connection).

- What do you wish the tool would do better?

  David wishes the tool they're using would integrate better with other tools.

  In addition, from previously mentioned dislikes, it seems that David wishes they'd be able to manage different workflows at the same time, such as the Studio XP's teacher's and developer's different workflows.

**5. Is it important for you to be able to see the status of a particular task, or to monitor the progress of your team as a whole?**

David finds it important to keep track of the progress of team members and the status of tickets.

- If so, do you have a process in place for this?

  David considers monitoring the team's progress and the status of tickets with Monday is good-enough for the size of Studio XP's development team.

- Do you feel the process works well for you and your team? Why, or why not?

  Monday contains a lot of information because the development team heavily rely on it. However, the teachers do not have much value for the different information available on Monday.

  Although they have tried to include the teachers with using Monday, the teachers were overwhelmed and were not relying on it. Instead, a different approach was taken by David, where the data is extracted from Monday and shared with the teachers on Slack, the Wiki, or through spreadsheets.

**6. By now, you may have an idea of what we have in mind for the tool we’d like to build. Do you think a tool like the Workflow Manager would be useful to your organisation?**

According to Daniel, the magical tool that Studio XP would be interested in is one that could easily integrate with other tools. More importantly, it is one that would bridge the difference in workflows and their rhythms and paces, and one that would manage the flow of information according to the needs of the teams.

**7. Do you have any last thoughts you’d like to chat about before we wrap up?**

N/A
