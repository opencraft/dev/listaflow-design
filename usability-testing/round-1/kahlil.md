# Quick links

- [User Interview Spreadsheet Notes](https://docs.google.com/spreadsheets/d/19L6V47JrMj42XaePnE-7PfVjodKkdkrMmoPjd_ozVGM/edit#gid=1408935478)
- [Usability Test Spreadsheet Notes](https://docs.google.com/spreadsheets/d/1N_0eabfj3UH1PrKKxXyrxlgZrMzZWJZnxvBaTvW_moY/edit#gid=1408935478)
- [User Interview Video](https://drive.google.com/drive/u/2/folders/182ujVgtToIQW1hqx1syRQbjH6lX92pSP)
- [Usability Test Video](https://drive.google.com/drive/u/2/folders/1Im03KrLlnvHNFFvRb5_ytOS9AEov0vdm)

We've included a summary of the User Interview and Usability Test below.


# User Interview Summary

**1. Can you tell me a bit about your role at OpenCraft?**

Kahlil refers to himself as a software developer and dev-ops person. As he is a newcomer, he is still learning the OpenCraft processes, and does not have many managerial responsibilities yet.

**2. What problem/s do you hope the Workflow Manager will solve for you?**

OpenCraft is very process-driven, and it takes time to get up to speed with all the different ways of working. Kahlil would like the Workflow Manager to speed up that process so that he can focus on getting stuff done (i.e. writing code and solving problems).

**3. Which part of the Workflow Manager would you expect to use most often, or find the most useful?**

Kahlil would find it useful for the Workflow Manager to free up some time so he can focus on his work. He would like it to take the thinking out of certain processes, and confirm that he's doing the right thing.

**4. Would you find it useful if certain checklists were combined with details from the OpenCraft handbook?**

He thinks it would be better to treat the handbook as the authoritative source and link to it. The handbook should be improved so that users following a handbook link, find what they're looking for. On the other hand, he does mention that it may be useful to include minor details from the handbook in the checklists (e.g. mention on the Vacation Checklist the number of days in advance for which leave should be requested).

**5. When communicating with other team members, do you rely more on email notifications, or in-app notifications?**

Kahlil tends to rely on his email to notify him of any Jira tasks needing his attention. He mentions that this ties into OpenCraft's asynchronous approach.

**6. How do you keep track of all the tasks that have been assigned to you? Are you satisfied with your current process?**

He recreates Jira tickets in Todoist and organises his schedule there. He has also set up recurring tasks inside Todoist. Althought the process works for him, he doesn't like the time it takes to transfer items from Jira to Todoist, nor the facet that he has to use 2 tools.

**7. Do you often check the progress of a task or team? What type of progress display do you find most useful?**

To limit distraction, Kahlil only checks the progress of tasks once or twice a day. He only really checks the progress of tasks for which he is responsible to ensure the people inolved are moving ahead on the task.

**8. Are there any tools you’ve used that you’d like to discuss? e.g. monday.com, Process Street, todoist, etc? Feel free to share your screen if you’d like to highlight specific aspects of the tool.**

- Todoist
- Jira
- monday.com
- Process Street

**9. Is there anything about the tool that you particularly like? If so, what?**

_Todoist_
- Kahlil uses todoist to organise his personal and work life. He mentions that working remotely means the two often overlap, so likes to combine his personal and work to-do's in one checklist (with the option to view them separately). He likes that his personal to-do's don't end up on work servers
- It's easy to prioritise tasks, and to focus on 1 task at a time
- It's quick and easy to break a task into subtasks
- Keyboard shortcuts make it very fast to add tasks
- Good natural language interpretation, so it's easy to add recurring reminders (e.g. "I want to do X every day at 9am")
- Easy to select a task's due date from they calendar dropdown
- Can quickly add tasks to the INBOX and expand upon them later. Gives him peace of mind that he's not going to forget to do it
- Can place today's tasks in the TODAY list (and link to the applicable Jira tickets from the task's title)
- Ability to split tasks into free-form subtasks
- Can organise tasks by dragging-and-dropping
- Can create your own categories like "Today's Frog" to list to-do's that are important to complete first thing in the morning.

_monday.com_
- Although Kahlil doesn't really use monday.com, he mentioned on the forum that “My Week” is his preferred view

**10. Do you dislike anything about the tool?**

_Todoist_
- Todoist does not make it easy to manage complicated recurring tasks or workflows
- Doesn't handle task dependencies well

_Jira_
- Kahlil finds the version of Jira that OpenCraft uses slow and clunky
- It's difficult to find things in Jira
- Creating subtasks is difficult and slow

_monday.com_
- Kahlil hasn't enjoyed working with monday.com. He feels it overcomplicates what he wants it to do i.e. provide simple checklists that takes thinking out of the process
- It takes long to figure out how monday.com has been organised, and how to use it
- Kahlil thinks the way checklists have been structured in OpenCraft's monday.com account is more complicated than the Google spreadsheet they were using before.

**11. Does the tool make it easy to set the status of a task? Why, or why not?**

On a day-to-day basis, Kahlil uses Todoist as a checklist. Only once a task has reached a certain point, does he visit Jira to update the status of the task.

**12. What do you hope the Workflow manager will do better than the tool?**

Kahlil thinks that to-do items should be less vague e.g. "Think about X this week". Instead, users should be able to visit a checklist and know straight away, "what is it that I need to do today?"

**13. Is there anything else about the tool that you’d like to mention?**

(Kahlil mentions a few points about the Vacation Checklist used in the usability test. We have added his notes to the usability test feedback below.)

---
<br>

# Usability Test Summary

We've added how the user experienced each item of the checklist below:

**1. Creating an instance**

Kahlil isn't sure how to create his own instance of the checklist, but after a few moments looking around, he gathers he needs to “run checklist” and selects it. When the page loads with a field for **Checklist name**, he takes a bit of time before giving it a title.

**2. About this checklist**

Kahlil reads through this and starts the checklist.

**3. Request vacation far enough in advance**

Kahlil thinks that a link to the relevant section in the handbook should be added to this section, as the checklist needs to be kept in sync with the handbook.

Kahlil checks the sub-task **Confirm my vacation request is far enough in advance**, but then seems unsure if he needs to select the green "Complete task" button as well. He assumes by clicking the button, it’ll check off the item in the main checklist. He notes that the "Next" button must be available to skip items in the checklist that cannot be completed.

**4. Check the team calendar**

Kahlil thinks it would be best to get a tentative "yes" from people that could backup his roles / rotations before adding his vacation to the calendar.

**5. Add vacation to OpenCraft calendar**

Kahlil skims the content of this task and doesn’t add the information in the correct format (ie. Kal’s Day Off (reduced hours 4h). He’s also unsure if he needs to add a description to the calendar event.

Kahlil only notices the example templates format (ie. John: off) after adding a calendar event. He mentions he should read all the content of a task first before actioning.

**6. Update epic reviewer/s**

Kahlil skips ahead to the next task as he says this task will take time to confirm. He notes that a lot of back and forth is required to update epic reviewers.

**7. Identify backup/s for all roles**

Kahlil skips ahead to the next task as he says this task will take time to confirm. He notes that a lot of back and forth is required to secure backups for roles.

**8. Identify backup/s for all rotations**

Kahlil skips ahead to the next task as he says this task will take time to confirm. He notes that a lot of back and forth is required to secure backups for rotations.

**9. Identify your vacation type**

Kahlil thinks that a vacation type should be selected before adding an event to the calendar.

**10. Announce your vacation on the forum**

At first Kahlil thinks this step should occur around the same time as adding the event to the calendar. He feels the steps are slightly out of order.

However, when he reads the task information he realises he needs to find people to cover for him before announcing it on the forum. He suggests that this task should be blocked until he's confirmed his vacation type, dates and who's covering for him.

Kahlil struggles to find the vacation announcement forum, but eventually see's there is a link to it in the task information.

**11. Link to the forum post from the calendar event**

Kahlil finds this step straight forward

**12. Complete or reassign your tasks and/or reviews**

Kahlil finds this step straight forward

**13. Send newcomer reviews early**

Kahlil thinks another task included this step. When he can't locate the tasks, he mentions that it should be covered in **Complete or reassign your tasks and/or reviews**.

**14. Set a vacation responder**

Kahlil thinks the handbook covers what should be added to the vacation responder (ie. a template email)

**15. Enjoy your vacation**

Kahlil adds a comment "Why thank you :)" and then selects "Complete Task". He let's out a whoop when he sees the confetti and then summises that's the end of the process.

<br>

**Questions:**

**1. Would you say the checklist was easy to use? Why, or why not?**

Fairly easy to use, even when not feeling the sharpest. Pretty intuitive. The purpose of checking of items was not 100% clear until you try it and see the progress bar.

**2. You may have noticed that some information from the OpenCraft handbook has been added to the checklist. What did you think about how the documentation and checklist have been combined?**

Some of that was good. I liked not having to check the handbook for minor details like the number of days in advance that vacations must be requested. Have to be careful that the handbook remains the authoritative reference. Definitely think there should be a link to the handbook whenever the information is duplicated.

**3. Did you feel that the checklist items were in the correct order? If not, what would you change?**

The order seems correct. At first I thought step 4 (Add vacation to OpenCraft calendar) was out of place, however, I then realized that by doing this early signaled an "intent" to make a vacation (before completing all the required steps) and would help to avoid conflicts.

**4. What did you dislike about the process of following the Vacation Checklist?**

You can complete a step without checking off all the items and checking off all items in a step does not complete the step. I feel there should be a constraint: complete button should be disabled until all check-boxes are ticked.

Not sure why we would want to make comments while running a checklist. Who is going to read the comments? We have many other communication channels and this seems redundant.

Some of the tasks descriptions were too verbose for the audience, especially step 4.

You can complete Step 9 (Announce your vacation on the forum) before the previous steps have been completed. There should be a hard constraint on that.

**5. What did you like about the process?**

I liked being able to skip ahead and complete some tasks before others. We have an asynchronous workflow and communications can be delayed.

<br>

**Other:**

**1. General notes**

Kahlil notices that tasks he doesn’t check off move to the bottom of the list. This confuses him.

While Kahlil is working on **Send newcomer reviews early**, he notices that he didn’t check off some subtasks within tasks. However the task has been checked off in the main checklist. This is because he selected the "Complete Task" button. He notices that tasks with subtasks have a green progress bar in the main checklist.

Kahlil says he did a bit of flapping around when completing the test, mostly because he doesn’t read things before starting to click.

He generally found the process mostly okay.

**2. Mentioned in user interview**

Kahlil brings up 2 points in his user interview (which was held after he completed the usability test):
- He feels there is too much text in some places on the Vacation checklist e.g. the text about setting up the calendar invite; it could simply say "name: X hours"
- There should be stronger dependencies between certain checklist items e.g. you shouldn't be able to check off the forum announcement item until everything else has been done "
