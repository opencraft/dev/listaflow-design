# Quick links

- [User Interview Spreadsheet Notes](https://docs.google.com/spreadsheets/d/19L6V47JrMj42XaePnE-7PfVjodKkdkrMmoPjd_ozVGM/edit#gid=2096943050)
- [Usability Test Spreadsheet Notes](https://docs.google.com/spreadsheets/d/1N_0eabfj3UH1PrKKxXyrxlgZrMzZWJZnxvBaTvW_moY/edit#gid=2096943050)
- [User Interview Video](https://drive.google.com/drive/u/2/folders/1RgMRmx9gNqi3PgEaW5omcC6Qk9EaX7RT)
- [Usability Test Video](https://drive.google.com/drive/u/2/folders/1rFBSnSIJ8ow0wiekUk_nTvEUvXL1N6Sv)

We've included a summary of the User Interview and Usability Test below.

# User Interview Summary

**1. Can you tell me a bit about your role at OpenCraft?**

Fox used to be a senior software developer, but is now the Business Development Specialist at OpenCraft. He helps to sell OpenCraft's services to other organisations. This involves sourcing leads people, convincing them that OpenCraft is the right choice for their project.

Because OpenCraft only considers serious candidates, there is a lot of filtering out of leads required. This takes effort, and means that there is often quite a bit of time before making a successful sale.

**2. What problem/s do you hope the Workflow Manager will solve for you?**

In the long term, Fox hopes that the Workflow Manager will be a good demonstration of how an asynchronous workflow can be managed well. He thinks this will especially be useful when OpenCrafts starts to market to other companies.

In the short term, he hopes the Workflow Manager will make tasks easier to accomplish, and that it will make it less likely for him to forget to complete certain tasks.

**3. Which part of the Workflow Manager would you expect to use most often, or find the most useful?**

Since Fox is new to his role, he doesn't yet know which part of the Workflow Manager he's likely to use most often. Over time, he says he'll have a clearer idea of which procedures would be better managed by the Workflow Manager (he mentions that they are currently using a CRM tool to manage leads, and that this might be an example of a workflow that could be better managed by the Workflow Manager).

**4. Would you find it useful if certain checklists were combined with details from the OpenCraft handbook?**

Fox thinks it would be useful to include certain details from the handbook in the Workflow Manager. He thinks this might encourage people to use the Workflow Manager sooner rather than later, meaning they'll get familiarised with it early on.

Fox mentions that newcomers often get overwhelmed by all the tools used at OpenCraft. He suggests adding an onboarding task to the Workflow Manager for newcomers to follow. This will not only help to onboard newcomers, but will also get them familiarised with the tool early on.

**5. When communicating with other team members, do you rely more on email notifications, or in-app notifications?**

Fox tries to use the communication tool that's most appropriate for the task he's busy with. However, because he's an extrovert, he finds he often uses chat, even when he should perhaps be using Jira (to allow people more time to respond).

For Fox, the choice depends on how quickly he needs an answer. If he needs an answer fast, he uses chat. When slower responses are fine, he uses Jira or another tool that sends out an email for you after a while.

Fox does a lot of direct emailing in his new role. In contrast to when he was a developer, he finds that as a sales representative, there are a lot of things that can't be contained in a Jira ticket (such as communicating with customers).

**6. How do you keep track of all the tasks that have been assigned to you? Are you satisfied with your current process?**

Fox uses Jira to manage his tasks. Although he finds he is able to keep track of his tickets, he says that some tasks often seem like a lot more overhead than they're worth. He gives the example of having to create a ticket to answer an email simply so that there's somewhere to track his time.

He also finds the current ticket template frustrating. He doesn't like the fact that you have to write up a whole user-story, even for simple tasks.

**7. Do you often check the progress of a task or team? What type of progress display do you find most useful?**

Fox frequently checks the sprint board in Jira to see what he has to do, or if there's anything for him to review; however, he's not a fan of the user experience. He feels there are a lot of tasks displayed that are not relevant to him. Although he can filter the list by his ""issues"", or his ""reviews"", he can't put both filters on at the same time as this elimates all the tasks. He knows he can create a custom filter to handle this, but hasn't done that yet.

The Jira sprint board includes tickets of all the members of the ""business cell"" (i.e. Fox, Gabriel, Xavier, Doug), even if they're part of the business board. Although it makes sense to include tickets from other categories or cells, there is an overwhelming number of tickets displayed on the sprint board. He wishes he could eliminate everything that's not relevant to him.

**8. Are there any tools you’ve used that you’d like to discuss? e.g. monday.com, Process Street, todoist, etc? Feel free to share your screen if you’d like to highlight specific aspects of the tool.**

- Process Street
- monday.com
- Jira

**9. Is there anything about the tool that you particularly like? If so, what?**

*Process Street*

- Fox likes that Process Street leads you through a process step-by-step so that you don't have to think about what you're doing. This removes a lot of overhead from his brain, allowing him to work more effectively.

*monday.com*

- Fox likes monday.com
- He finds it allows you to constrain a workflow, and make it work for your needs
- Fox likes the silly things that monday.com has added (e.g. the Llama Farm). He says it makes you want to play with the tool
- Fox likes the gifs that show when you complete a task. He says this adds to the feeling of accomplishment when you finish a task, and makes users want to hit the ""complete"" button

**10. Do you dislike anything about the tool?**

*Jira*

- Fox feels slowed down by the fact that he has to create a new Jira ticket every time he does something. He feels this disincentivises him from doing the task
- He feels that Jira is made for managers, and not for the individual worker
- Although Jira can technically work for any workflow, Fox says this takes a lot of customisation. He thinks that most users don't feel the need to use the customisation options, and that if they do, they don't have the time to implement them
- For Fox, Jira's big flaw is that it tries to do everything
- Jira has no sense of fun

*monday.com*

- Fox feels that, like Jira, monday.com has the problem of trying to be everything at once. However, he feels they manage this better than Jira does

**11. Does the tool make it easy to set the status of a task? Why, or why not?**

*monday.com*

- Fox finds it easy to set a task's status in monday.com

*Jira*

- Fox says it's relatively easy to set the status of a task in Jira, as it simply requires pressing a button, or dragging a task to the next column. He finds this process easier than some other tools he has used in the past, which simply relied on email

**12. What do you hope the Workflow manager will do better than the tool?**

Fox would like the Workflow Manager to show him only the most relevant information.

He would also be happy if he could spend less time on tooling (which he currently spends a lot of time on).

**13. Is there anything else about the tool that you’d like to mention?**

Fox says that the Workflow Manager should be fun to use. It should make users feel like they're being rewarded in some way for using it - not only through the intrinsic reward of a fun user experience, but also by (for example) awarding users trophies or badges when they complete a task. Fox thinks this will help incentivise users to complete tasks.

Fox thinks it would be great if the Workflow Manager could do useful things for users (for example, if the vacation checklist could automatically block out your calendar for you). He feels this would incentivise users even more, as they'd immediately see the benefit of using the tool.

Fox says that the Workflow Manager should feel like it fits naturally into the flow of the task with which the user is currently busy. The less the tool disrupts the user's flow, the better.

**14. Do you have any last thoughts you’d like to chat about before we wrap up?**

Not only does OpenCraft want to sell the Workflow Manager to other companies, but they also want to become a leader in the asynchronous workflow space. For this, they need a tool that effectively demonstrates the benefits of and asynchronous workflow.

---
<br>

# Usability Test Summary

We've added how the user experienced each item of the checklist below:

**1. Creating an instance**

After clicking the link, Fox's eyes and mouse gravitate towards the green "Run Checklist" button. Fox gets distracted though by the "About this checklist" section. Soon after, Fox tries to run the checklist.When asked to name the checklist, Fox mentions that the checklist's default name is a little bit weird.

**2. About this checklist**

Fox read this section before running the checklist. Fox found it helpful because it mentions some key information regarding not needing to "fill it out" if one's planning to travel and still work, also regarding the existence of a different checklist if a person is sick.

After running the checklist, Fox shows some frustration when faced by the "About this checklist" again, mentioning "that's what I just read". Fox proceeds to click Next.

**3. Request vacation far enough in advance**

Fox reads the section and clicks the checkbox to confirm the vacation requested is far enough in advance. Fox's mouse then jumps to the green "Run Checklist" button; however, Fox doesn't directly press the button.

**4. Check the team calendar**

Fox likes that there's a link to the team calendar. Fox mentions that the "less than 20% of my cell is off on my vacation days" isn't applicable in his case because there are only 3 or 4 members in the business cell, which means a single member off would account to more than 20% of the cell being off.

It is important to note that the checklist is intended for developers, which aren't in the business cell.

Fox hovers over the "Complete Task" button, but instead presses the "Next" button. This leaves the task "unchecked" or incomplete in the sidebar.

**5. Add vacation to OpenCraft calendar**

While reading the instructions, Fox realizes that he didn't follow that format with the vacation he requested for August and updates the vacation event to match the required format.

Fox mentions that he didn't do the checklist properly, he just marked it on the calendar.

Fox then seems to jump over the description and focuses on the examples. Fox highlights that there is no other Fox on the team, so he won't need to make any more adjustments.

**6. Update epic reviewer/s**

Fox mentions that the epics that he's responsible for should be completed by the time the vacation arrives. Fox then ticks off the checklist items and completes the task.

**7. Identify backup/s for all roles**

After reading the section, Fox mentions that he should send an email to Gabriel in the business cell to verify that he's able to cover for him during him absence.

Fox chooses to communicate with Gabriel over email instead of Jira.

Fox assumes Gabriel will say yes for the sake of the video, checks the checkbox, and completes the task.

**8. Identify backup/s for all rotations**

Fox mentions that he doesn't have any rotations currently. Fox then ticks off the checklist items and completes the task.

**9. Identify your vacation type**

Fox quickly goes over the section and proceeds to the next task.

**10. Announce your vacation on the forum**

Fox opens the forum to verify that he did ask for time off. Once he verifies he has posted about the days off during August, he goes back to the checklist.

Fox then scrolls down to the example template and goes back to the forum post. He mentions that he would update it to mention that Gabriel is his backup during that time, but since he hasn't gotten the "yes" from Gabriel, he'll refrain from doing that just yet.

The quote/code blocks in the examples given and the "Complete Task" button seem to catch Fox's attention. Fox seems to heavily rely on the examples to gain the information he needs rather than reading through the text.

**11. Link to the forum post from the calendar event**

After reading the task, Fox grabs the link to his forum post and links it on the Calendar event.

Although the forum post mentions 2 vacations, one in July and one in August, Fox only adds the link to the vacation event during August.

Fox mentions that he hasn't worked through the entire checklist before.

**12. Complete or reassign your tasks and/or reviews**

After reading through the task, Fox checks of checklist items and completes the task. Fox justifies it as being too far ahead and that it conflicts with the deadline of this video.

**13. Send newcomer reviews early**

Fox mentions he isn't a core member, so he cannot do newcomer reviews. Fox completes the task.

**14. Set a vacation responder**

Fox mentions that he cannot do that until he's about to leave. Fox completes the task enthusiastically, assuming that the checklist is done. Fox then seems to be a little bit disappointed.

**15. Enjoy your vacation**

Fox realizes that the last checklist item doesn't contain any information. While mentioning that he can't complete the "Enjoy your vacation" task until he's on the vacation, he presses the button.

Fox seems unsatisfied and disappointed that there was no celebratory text after completing the checklist. He mentions that a hovered text mentioning "You've completed this checklist" would've been nice to have.

However, it is important to note that the reason there was no confetti is because the checklist wasn't actually complete. The "Check the team calendar" wasn't completed, if you check the sidebar during the video.

<br>

Questions:

**1. Would you say the checklist was easy to use? Why, or why not?**

Fox states that the checklist was easy to use.

Fox mentions that it was straight forward.

Fox mentions that "one or two" tasks were a bit redundant, such as "decide which kind of vacation you're on". Fox states that it didn't have to be separate from other tasks, but doing this extra step doesn't hurt.

He mentions that to complete the checklist, one would have to go back and forth to complete the checklist. So Fox requests that the checklist nags the assignee to remind them to finish it before they go. He expects people to leave while the checklist is only half way finished if no reminders are sent.

**2. You may have noticed that some information from the OpenCraft handbook has been added to the checklist. What did you think about how the documentation and checklist have been combined?**

Fox mentions that having the documentation and the checklist together is really helpful.

Fox asks to have the Handbook point to the necessary checklists, and have the checklists contain the canonical version of the documentation. Fox mentions the importance to avoiding redundant information and mentioning the same information in two places at once. Fox continues to say that the information should be mentioned in a single place so that when the information is updated, it wouldn't have to be updated in multiple places.

**3. Did you feel that the checklist items were in the correct order? If not, what would you change?**

Fox has trouble to remember the order of the items and decides to take a look at the checklist again.

Fox mentions that the tasks seem to be in the right order.

Fox, then, realizes that the "Check the team calendar" task wasn't "checked". He proceeds to "check it off" using the checkbox on the sidebar. The confetti appears, then, Fox cheers for the celebratory confetti.

Fox mentions that he missed checking if the task was actually completed. Fox thinks that he might have gotten confused.

It seems like Fox expected the task to be "checked" or completed after checking off the "Check the team calendar" and "Confirm that less than 20% of my cell is off on my vacation days" checklist items.

**4. What did you dislike about the process of following the Vacation Checklist?**

Fox disliked that he had to go back and forth with the checklist. He acknowledges that this might not be avoidable; however, he mentions that the reminder emails would be help him continue thte checklist from where he left off.

**5. What did you like about the process?**

Fox likes that the process was really straight forward and that the user only gets to advance once they've acknowledged what they need to do one step at a time.

<br>

Other:

**1. General notes**

N/A

**2. Mentioned in user interview**

N/A


