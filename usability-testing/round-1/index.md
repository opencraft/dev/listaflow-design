# Feedback From Usability Testing (Round 1)

## 1. Feedback From Remote Usability Test

> Although there are 5 participants in the round 1 usability tests of March 2021, there are only 4 remote usability tests, because David (an external user) did not do a remote usability test.

### Findings Summary

#### Frustrations

- 2 users said that there was too much information accompanying the checklist items
- 2 users said that it wasn't obvious how to start a "run" of a checklist in Process Street

#### Delights

- 3 users liked that the documentation for completing an item was available in the item
- 3 users liked the confetti shown when a checklist is complete
- 3 users learnt something new about the vacation announcement process
- 1 user liked that it was clear which items in the process were complete and incomplete

#### Behaviors

- 2 users referenced the documentation in the checklist item to resolve uncertainty

#### Other notable findings

- being able to reorder checklist items would be an important feature to implement
- item dependencies or blockers would be an important feature to implement
- 2 users updated the content provided in the example templates after pasting into Discourse
    - being able to suggest or request improvements on checklist items may be important
- 2 users showed interest in some sort of reminder for tasks that could not be completed immediately
- 2 users requested the software be aware of the checklist items inside a task to auto-complete a task or prevent its completion.
- conditional logic may be beneficial to reduce the number of checklist items a user is required to complete (refer to Process Street's conditional checklist item feature)

### Links

- [Test instructions][test-instructions]
- [Summary Spreadsheet Notes][usability-test-report-analysis]

---

<br>

## 2. Feedback From User Interviews

### Findings summary

#### Frustrations

- 4 users said that OpenCraft processes are complex or time-consuming
- 3 users said that missing tasks when completing processes is currently a concern
- 3 users said that Jira is slow
- 2 users said that monday.com is more complex to use than a simple spreadsheet

#### Desires

- 3 users said they want processes to be faster to complete
    - users mentioned spending less time in tooling, or more time focusing on actual work
- 3 users want it to be easy to see the status or progress of their personal tasks, as well as team tasks
- 2 users said that they want to take the thinking out of completing processes

#### Other notable findings

- 1 user said that having to create tickets to track time disincentivises doing the work
- 1 user said there's overhead in writing user stories for every issue, when some issues only need a simple explanation
- 1 user said that they found Process Street good for taking the thinking out of doing tasks

### Links

- [User interview script][user-interview-script]
- [External user interview script][external-user-interview-script]
- [Summary spreadsheet notes][user-interviews-report-analysis]

---

<br>

## 3. Test-User-Specific Notes

- [Nizar](./nizar.md)
- [Sid](./sid.md)
- [Kahlil](./kahlil.md)
- [Fox](./fox.md)
- [David (Studio XP)](./david.md)

---

<br>

## 4. Suggestions Based on Findings

There appears to be a strong desire to make it easier to follow processeses at OpenCraft, so that team members may spend less time on processes and tooling, and more time focussing on their work.

Another observation was that easy-to-use checklists would reduce the amount of thinking required when completing processes, and ensure that no steps are missed.

As such, when working through these kinds of tasks, they should:
- be easy to complete
- require little thought
- leave little room for steps to be missed

Another recurring theme was the OpenCraft handbook, and how it should be used with the Workflow Manager. While users felt that the handbook should remain the primary source of information for processes, it was evident from the testing that there is an advantage to having documentation available within the checklist itself. However, effort would need to be put into evaluating the risk of duplication, stale information, and additional maintenance overhead of documentation residing in multiple places.

A number of users mentioned that they like the simplicity offered by a simple checklist, as opposed to the user experience of monday.com. They also felt that tools like monday.com try to do too much. With these points in mind, it’s important to keep the UX of completing checklists as simple as possible in the Workflow Manager.

#### Suggestions

- Determine the role of the documentation in the handbook versus the documentation in the Workflow Manager, specifically regarding the level of detail that should be included
- Provide a dashboard where users can easily find items that are relevant to them
- Make it easy for users to view the status of their checklists
- Make it clear to users which tasks they still need to complete
- Give users an overview of their progress, as well as the progress of their team
- Because users seem to favour the simplicity of a checkbox, make this the primary method of progressing through tasks
- Make sure the application works fast, and that it doesn’t disrupt a user’s flow
- When building features, a guiding principle should be that the Workflow Manager occupy as little of users’ time as possible
- Add an element of delight to the Workflow Manager so that users enjoy using it

<!-- Links -->

[test-instructions]: https://gitlab.com/opencraft/dev/workflow-manager/-/blob/master/docs/usability-testing/remote-usability-tests-march-2021.md
[usability-tests-report]: https://docs.google.com/spreadsheets/d/1N_0eabfj3UH1PrKKxXyrxlgZrMzZWJZnxvBaTvW_moY/edit#gid=977594581 "Usability Tests Report"
[usability-test-report-analysis]: https://docs.google.com/spreadsheets/d/1N_0eabfj3UH1PrKKxXyrxlgZrMzZWJZnxvBaTvW_moY/edit#gid=2043680478 "Usability Tests Report - Analysis"
[user-interviews-report]: https://docs.google.com/spreadsheets/d/19L6V47JrMj42XaePnE-7PfVjodKkdkrMmoPjd_ozVGM/edit#gid=289181425 "User Interviews Report"
[user-interviews-report-analysis]: https://docs.google.com/spreadsheets/d/19L6V47JrMj42XaePnE-7PfVjodKkdkrMmoPjd_ozVGM/edit#gid=2043680478 "User Interviews Report - Analysis"
[external-user-interview-script]: https://gitlab.com/opencraft/dev/workflow-manager/-/blob/master/docs/usability-testing/external-user-interview-march-2021.md "External User Interview Script"
[user-interview-script]: https://gitlab.com/opencraft/dev/workflow-manager/-/blob/master/docs/usability-testing/internal-user-interviews-march-2021.md "User Interview Script"

