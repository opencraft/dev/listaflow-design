# Quick links

- [User Interview Spreadsheet Notes](https://docs.google.com/spreadsheets/d/19L6V47JrMj42XaePnE-7PfVjodKkdkrMmoPjd_ozVGM/edit#gid=705693488)
- [Usability Test Spreadsheet Notes](https://docs.google.com/spreadsheets/d/1N_0eabfj3UH1PrKKxXyrxlgZrMzZWJZnxvBaTvW_moY/edit#gid=705693488)
- [User Interview Video](https://drive.google.com/drive/folders/1lSeDwV_0qnVT36NLY__nweylPPq7HMBi?usp=sharing)
- [Usability Test Video](https://drive.google.com/drive/folders/1mQCdAimS2PekuVoJ-jcmSLNrKq7Sp6ce?usp=sharing)

We've included a summary of the User Interview and Usability Test below.

# User Interview Summary

**1. Can you tell me a bit about your role at OpenCraft?**

Nizar is an open source developer. He is also a sprint planning manager and epic owner at OpenCraft. As such, he is responsible for planning tickets for sprints, and for projects as a whole. 

**2. What problem/s do you hope the Workflow Manager will solve for you?**

Nizar hopes it will help guide him through new processes until he becomes used to them (he finds that the handbook has too much information for this). He'd like the Worklow Manager to serve as a reference for him where he can ensure he doesn't miss anything.

**3. Which part of the Workflow Manager would you expect to use most often, or find the most useful?**

He thinks he'll use it mostly as a reference while he gets used to certain processes.

**4. Would you find it useful if certain checklists were combined with details from the OpenCraft handbook?**

Nizar seed the handbook and the Workflow Manager existing independently, but working closely together. He suggests including the source code from the handbook in the checklist to avoid the need for copying items manually.

**5. When communicating with other team members, do you rely more on email notifications, or in-app notifications?**

Nizar finds email notifications suitable for tasks that need to be replied to within the next 24 hours. He relies on in-app notifications for anything that needs to be addressed more urgently.

**6. How do you keep track of all the tasks that have been assigned to you? Are you satisfied with your current process?**

In the middle or towards the end of each sprint, Nizar goes through all his tasks and determines which are high priority. This helps him to map out his whole week. At the beginning of each day, he uses a notebook to plan out his day. He prefers to do this manually as opposed to digitally, as it forces him to look away from his screen, and gives him time to think about each task. This process has helped him avoid a lot of spillovers

- For monday.com tasks, Nizar relies on the email notifications that are sent everyday to help him determine what he needs to do today
- For Jira tasks, Nizar uses the Active sprint board to keep track of tasks.

**7. Do you often check the progress of a task or team? What type of progress display do you find most useful?**

For Nizar, it would be nice to have a view that shows both personal, and team progress.

When it comes to team management, some tasks have allocated reviewers. These reviewers help ensure that no one is blocked, and Nizar believes this allows for a smoother process. He feels monday.com is geared more to personal task management, and only occasionally references progress here. He has his own methods when it comes to personal task management.

**8. Are there any tools you’ve used that you’d like to discuss? e.g. monday.com, Process Street, todoist, etc? Feel free to share your screen if you’d like to highlight specific aspects of the tool.**
- monday.com
- Process Street

**9. Is there anything about the tool that you particularly like? If so, what?**

_monday.com_
- Nizar finds monday.com nice to work with
- It indicates nicely whether the timeline was met or not
- Status and Timeline are linked which is helpful, because the My Week view shows what tasks need to be done, and which are coming soon
- We finds the My Week view more helpful than the Workspace board
- He likes monday.com's flexibility
- The Mirror functionality is useful for connecting different tasks
- The columns in Workspace view keep things modular
- The Form view allows users to populate table items quickly

_Process Street_
- Nizar loves the product, and says it's beautiful

**10. Do you dislike anything about the tool?**

_monday.com_
- Nizar finds monday.com slow and laggy 
- The Workspace interface is "heavy" because of all the flexibility the tool offers
- The documentation doesn't easily allow you to understand how to use a feature
- Nizar says that he misses his spreadsheet sometimes because it didn't lag. It also simple showed the task title, description and status (check mark)

_Process Street_
- Feels it doesn't give developers the freedom to automate things
- He would prefer the option to choose when to see the documentation, so that he only sees it when he needs to

**11. Does the tool make it easy to set the status of a task? Why, or why not?**

Although monday.com allows you to set the status of the task, Nizar prefers the simple check mark Excel offers to show whether a task is done or not.

---
<br>

# Usability Test Summary

We've added how the user experienced each item of the checklist below:

**1. Creating an instance**

Nizar mentions that he has experience with Process Street. He thinks that some of his colleagues may not know that they need to select "Run Checklist" to start the checklist process. He didn't find this straight forward when first using Process Street.

**2. About this checklist**

Nizar clicks on the link "sick days checklist" and thinks it's another checklist that he should potentially run through. He sees it's just for testing purposes when the link opens.

**3. Request vacation far enough in advance**

Nizar thinks he needs to add his vacation information to the comments field on this step. He adds quite a lot of information to the comments field that includes the following information:
- Number of days he's taking off 
- The sprint he won't be available for
- The dates he's requesting for vacation, indicating that he's requested far enough in advance

Once Nizar has completed his comment, he selects "Complete Task". He forgets to check-off the subtask, but does so after an error brings this to his attention.

**4. Check the team calendar**

Nizar says it's nice that there is a link to the team calendar in the checklist. 

After looking at the calendar and confirming no one wants to take leave at the same time as him, he notes this in the comments field. Nizar checks the two subtasks before selecting the "Complete Task" button this time.

**5. Add vacation to OpenCraft calendar**

Nizar adds an event in the correct format (ie. Nizar: off). He seems unsure of which calendar to add the event to and thinks he should add it to his personal calendar, or his cell calendar. He reads the checklist instructions again and sees he should add it to the Opencraft calendar.

He mentions it's nice to have example templates in the checklist to make sure you're formatting content correctly.

**6. Update epic reviewer/s**

Nizar logs into his Jira account and goes to the Epics Serenity board. He talks through what he would need to do in order to update epic reviewers. 

**7. Identify backup/s for all roles**

Nizar mentions he is a mentor for a newcomer, and will need to identify a back up. He notes other OpenCraft members who can back him up for his other roles.

**8. Identify backup/s for all rotations**

Nizar opens the spreadsheet with the weekly rotations schedule and notes he's not on discovery duty and he's not a firefighter over his planned vacation time. So he checks the subtasks listed in the checklist item.

**9. Identify your vacation type**

Nizar easily notes which vacation type he's choosing. He doesn't add this to the comment field this time and just selects "Complete Task".

**10. Announce your vacation on the forum**

Nizar copies the forum announcement template. He says it's nice that there is a template. He selects the forum link in the checklist and then pastes the template into the thread. 

He jumps between the forum, the calendar and the checklist information to ensure he's entering the correct dates.

**11. Link to the forum post from the calendar event**

Nizar is unsure where to add the forum link in the calendar event. He notes it isn't mentioned in the checklist information. He decides to add the link in the event Location field, but then moves it to the event's description. He feels that makes more sense.

**12. Complete or reassign your tasks and/or reviews**

Nizar opens Jira and talks through the tasks that he'll need to complete or reassign.

**13. Send newcomer reviews early**

Nizar notes that he has a newcomer review and mentions who he would reassign the task to. He says he'll also prepare his thoughts on the newcomer and share it with team before he goes on vacation.

Nizar also notices this checklist item includes a link to the newcomer section in the handbook. He says this will provide any help he may need.

**Note:** Nizar doesn't recall that he mentioned newcomer reviews in the task: **Identify backup/s for all roles**. It's almost as if he assumed this task should be accomplished in **Identify backup/s for all roles** item.


**14. Set a vacation responder**

Nizar says he doesn't think he knows how to add a vacation responder in Gmail. He says it's nice that the checklist includes a link to the applicable instructions on how to do this.

He manages to add a vacation responder easily.

**15. Enjoy your vacation**

When Nizar completes the checklist he laughs at the confetti that is shown and says that's nice. He closes the checklist and goes back to the test instructions.

<br>

**Questions:**

**1. Would you say the checklist was easy to use? Why, or why not?**

Nizar finds the checklist easy to use, because the checklist includes detailed information about each step. 

He feels he had a few issues, one being updating his vacation responder. He wasn't aware that he needed to do this and was glad the checklist walked him through it. He also wasn't sure if the dates were inclusive or not, and where to post the forum link in the calendar event.

**2. You may have noticed that some information from the OpenCraft handbook has been added to the checklist. What did you think about how the documentation and checklist have been combined?**

Nizar loves how the checklist and documentation were combined, but also dislikes it at the same time.

He loves it because anything he needed, whether it was a template or information, was easy to refer to. 

He found the documentation was too generic though. It covered information for people that have roles, epics and reviews. Sometimes this information isn't valid for a certain person. He wouldn't want to go through steps that don't pertain to him.

**3. Did you feel that the checklist items were in the correct order? If not, what would you change?**

Nizar thinks it would be better to post to the forum before creating an event. 

He thinks it would be better to complete **Update Epic Reviewers**, **Identify backup/s for all roles**, and **Indentify backup/s for rotations** before adding an event and posting to the forum. He sees these three tasks a "blockers". 

Generally he found the checklist in the correct order though.

**4. What did you dislike about the process of following the Vacation Checklist?**

Nizar dislikes the excess of information. He feels the checklist was too long. 

He says the documentation could be shorter. He duplicates the template to show us how.

Just below "About this checklist", he creates a new task called "Plan the vacation". He suggests adding dropdown fields to ask the user if they have:
- any epics in development
- any roles in their cell (ie. yes or no)
- what roles the user is responsible for in their cell (ie. sprint manager, sprint planning manager, recruitment manager)
- if they have any firefighter or discovery duty rotation

He adds conditional logic to these form fields, to hide the epic reviewers, roles and rotations if not selected by the user.

The idea is to eliminate certain parts of the checklist that are not relevant to the user, based on the questions asked.

**5. What did you like about the process?**

Nizar loves going through processes that are well documented, like the Vacation Checklist.

<br>

**Other:**

**1. General notes**

N/A

**2. Mentioned in user interview**

N/A


