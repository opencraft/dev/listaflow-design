# Quick links

- [User Interview Spreadsheet Notes](https://docs.google.com/spreadsheets/d/19L6V47JrMj42XaePnE-7PfVjodKkdkrMmoPjd_ozVGM/edit#gid=558470124)
- [Usability Test Spreadsheet Notes](https://docs.google.com/spreadsheets/d/1N_0eabfj3UH1PrKKxXyrxlgZrMzZWJZnxvBaTvW_moY/edit#gid=558470124)
- [User Interview Video](https://drive.google.com/drive/u/2/folders/1CcYvz7rnrVgQ_Z4rW4eVE538s9R1n04V)
- [Usability Test Video](https://drive.google.com/drive/u/2/folders/1F5ElhHmjh3Or0RZwCjEOQF0bheBhqlgW)

We've included a summary of the User Interview and Usability Test below.

# User Interview Summary

**1. Can you tell me a bit about your role at OpenCraft?**

Sid works on development tasks and sprint planning at Opencraft. He says he mostly works through Jira backlog.

**2. What problem/s do you hope the Workflow Manager will solve for you?**

Sid finds Opencraft's sprint planning more complicated than what he's known before.

He hopes the Workflow Manager will help organise his tasks in a better way, specifically showing him a daily breakdown of tasks. Sid finds it easy to miss tasks, that he needs to do.

**3. Which part of the Workflow Manager would you expect to use most often, or find the most useful?**

Sid would find it useful if the Workflow Manager shows him his task list in a prioritised order. He'd also find it valuable if the Workflow Manager makes it easy to view his task deadlines.

**4. Would you find it useful if certain checklists were combined with details from the OpenCraft handbook?**

Sid sees the Opencraft handbook as a canonical source of information. He also mentions it will be valuable if the handbook shows a history of updates so it's easier to see what has been changed.

Sid mentions that perhaps the handbook shouldn't include steps to complete certain things. This is where the Workflow Manager should step in.

**5. When communicating with other team members, do you rely more on email notifications, or in-app notifications?**

Sid mostly relies on email notifications from Jira and emails between Opencraft employees. He finds that he has to make a point of ignoring the influx of email notifications when working on specific tasks, so as not to get distracted.

Sid notes that he hasn't enabled Monday.com email notifications.

**6. How do you keep track of all the tasks that have been assigned to you? Are you satisfied with your current process?**

Sid uses Jira to keep track of his tasks.

**7. Do you often check the progress of a task or team? What type of progress display do you find most useful?**

Sid doesn't check the progress of a task unless there a specific sprint update.

**8. Are there any tools you’ve used that you’d like to discuss? e.g. monday.com, Process Street, todoist, etc? Feel free to share your screen if you’d like to highlight specific aspects of the tool.**

- Todolist 
- Trello 
- Jira 

**9. Is there anything about the tool that you particularly like? If so, what?**

_Jira_
- Sid likes that Jira uses an open API
- He thinks Jira has a good filtering system
- Although he finds Jira has a lot of friction when adding tasks, this helps him to enter details more carefully (ie. lots of form fields)

_Monday.com_
- Sid likes how Monday.com shows the status of each team on sprint boards

**10. Do you dislike anything about the tool?**

_Jira_
- Sid doesn't like how long it takes to add tasks to Jira, although he does partly see this as a benefit, as mentioned above
- Sid finds the version of Jira that OpenCraft uses slow
- He thinks Jira email notifications can be a bit noisy

_Monday.com_
- Sid doesn't like how monday.com's sprint boards are sorted and structured.
- He finds it difficult to see which tasks are required to be completed for a specific day. This is because the tasks are grouped poorly in the weekly list.

_Todolist_
- Sid uses todolist to organise his personal life, but finds it can become cluttered

_Trello_
- Sid doesn't like Trello and mentions it's not powerful enough for what he needs

**11. What do you hope the Workflow manager will do better than the tool?**

Sid hopes the Workflow Manager will show him a single view that displays:
- Personal tasks: he'd like these to be categorised into smaller lists to make it easier to digest, eg. pending, backlog
- Team tasks: so he knows what the team is working on 


---
<br>

# Usability Test Summary

We've added how the user experienced each item of the checklist below:

**1. Creating an instance**

Sid knows to "Run Checklist" in order to create his own instance of the Vacation Checklist. When naming his checklist, he wonders whether he'll need to create a new checklist each time he wants to go on vacation. He assumes so, and adds the month of the vacation into the title.

**2. About this checklist**

Sid skims through the text in the "About this checklist" section, but doesn't seem to need to read it very closely. 

**3. Request vacation far enough in advance**

Sid selects the checkbox to confirm that his vacation request is far enough in advance, but is confused when this doesn't automatically complete the task. He clicks "Next" instead of "Complete Task", and is surprised that the checkbox in the sidebar doesn't automatically get checked.

**4. Check the team calendar**

Sid says the link provided in the checklist is wrong; it opens up the OpenCraft calendar, whereas users record vacations in their own individual calendars. He opens his own calendar and sees that no one else in his cell has booked the same days off. He ticks the checkboxes confirming that he's checked the team calendar, and that less than 20% of his cell is off over his vacation period. Again he selects "Next" instead of "Complete Task", and expects the task to be marked complete. When it isn't, he navigates back to select the "Complete Task" button.

Sid wonders what happens if he doesn't complete a task and just selects "Next".

**5. Add vacation to OpenCraft calendar**

At first Sid seems to know the format in which to add his leave to the calendar, so he only scans the instructions very quickly. However, before saving his calendar event, he goes back to check the example formats in the instructions.

**6. Update epic reviewer/s**

Sid takes a few seconds to find an epic in Jira. He mentions that he used to have a section on his Jira dashboard listing his epics to make them easier to access. 

He opens an epic and checks who the reviewer is. When he sees that the reviewer is Xavier, he says he'd contact him to handle recruitment, and would contact any of the other people he needed to. He selects the checkboxes confirming the epic reviewer can handle the take when he's away, and that he has transferred the applicable knowledge. He decides to select "Next" again instead of "Complete Task" to see what happens.

He notices for the first time that there's an option to add a comment. He tries to ping Xavier, but notices that "@xavier" doesn't bring up a list of users. He decides to ping "@test user" and adds a test comment. He notices that a speech bubble icon appears next to the task in the sidebar. 

For the first time, he selects the next task from the sidebar as opposed to using the "Next" button.

**7. Identify backup/s for all roles**

Sid selects the "roles" link in the instruction text, but as soon as he sees that it opens up the handbook, he doesn't seem to need to refer to it; he knows what his role was supposed to be for the epic. 

**8. Identify backup/s for all rotations**

Sid mentions that he really doesn't like how the "Complete Task" and "Next" options at the bottom of each task work. He ticks both checkboxes to see whether the "Complete Task" and "Next" items are affected at all, but they aren't. He unchecks the checkboxes and continues. 

He visits the "rotations" link and checks the spreadsheet that appears. He notices that he doesn't have any rotations for the sprint in question, so there is no backup needed. He checks the checkboxes confirming that he has selected a backup, and has confirmed that they can take over his rotations while he's away.

**9. Identify your vacation type**

Sid reads the definitions of each vacation type, and is interested in the "scoped time off" option; he has never noticed it before. He moves on to the next task.

**10. Announce your vacation on the forum**

Sid reads the instructions quite closely. He notices the words "vacation application" and mentions that it used to be an "announcement", not an application. He is not fond of the idea of having to apply for vacation.

Sid visits the forum link and uses the sidebar to scroll down. He mentions that he really doesn't like Discourse's sidebar. 

He goes back to the checklist and copies the text from the example template. He pastes it into his forum post and suggests the template includes the reason for taking leaving i.e. "I'd like to take full time off from Oct 27 - Nov 4 because [REASON]".

**11. Link to the forum post from the calendar event**

At first Sid is confused that he needs to open the calendar event again, but then says that it's a good task to include. He copies the link to the forum post and assumes he should paste it into the description of the calendar event.

**12. Complete or reassign your tasks and/or reviews**

Sid is slightly confused by the instruction text where it says, "At least a few days before you leave for you vacation..." He assumes this means he should put this task on hold until then. He tries to "Complete Task" without selecting any checkboxes but receives an error: "Oops, 1 field(s) still need to be completed." This confuses him. He mentions there are 3 fields, not 1. He tries to select just the first checkbox ("complete all tasks, or assign them to someone else"), but receives the same error when he tries to submit. He's confused so goes back to the instructions to see if he missed something. That doesn't help.

He ticks the first checkbox, but leaves the other two unselected as he says it's too early to confirm whether there are any spillovers, or if there are any tasks assigned to him while he's away. He selects "Next".

**13. Send newcomer reviews early**

Sid reads the instructions and seems interested to read that he should send newcomer reviews the sprint before his vacation.  He selects "Next" without opening the link to the handbook.

**14. Set a vacation responder**

After reading, "Set a vacation responder the day before you leave", Sid mentions that this could be done preemptively. He opens the instructions for setting a vacation responder in Gmail in a new tab (but doesn't ever refer to them). He quickly works out how to set a vacation responder. He moves on to the task.

**15. Enjoy your vacation**

Sid reads "enjoy your vacation" out loud and says, "yay!", but is confused about what is supposed to happen next. He seems to expect a message to appear, or something to happen. When it doesn't, he guesses that he is supposed to come back to the checklist at a later stage to finish the incomplete tasks. 

He goes back to the unchecked tasks in the sidear, and checks one of them. Nothing happens. He tries to navigate away from the checklist by clicking on the Process Street logo but again nothing happens. He decides to remove the path from the URL, and is directed to the signup/login page. He says he was expecting that there'd be some kind of a dashboard that his checklist would be saved to. He says he'd expect it to mention that there are 2 incomplete tasks that should be completed before he goes on vacation. 

Because there is no dashboard, he wonders if he can add some kind of a reminder to the checklist about the tasks that still need to be completed.

<br>

**Questions:**

**1. Would you say the checklist was easy to use? Why, or why not?**

Sid says the step-by-step listing of tasks was easy. He liked that there were premade templates included. 

He liked that he could see the documentation alongside the checklist, and that it included links and subtasks. However, he assumed that if he had completed all the subtasks in a step, the task would be automatically completed. 

He suggests that the link to the team calendar could perhaps be customisable based on who the user is.

**2. You may have noticed that some information from the OpenCraft handbook has been added to the checklist. What did you think about how the documentation and checklist have been combined?**

Sid says this was amazing. He likes that the documentation is combined with a step-by-step checklist as this prevents him from forgetting any steps. He mentions that he's forgotten items in the past because he missed steps from the handbook. He likes that the unchecked items remind him that he still has steps to complete.

**3. Did you feel that the checklist items were in the correct order? If not, what would you change?**

Sid says the order was ok. He thinks it would be easier to move the **Add vacation to OpenCraft calendar** step below the **Announce your vacation on the forum** step so users don't have to open their calendars again to add the forum link. He thinks the two steps could be combined into one: **Add a vacation and add a link**.

**4. What did you dislike about the process of following the Vacation Checklist?**

Sid is confused about where the checklist is being saved. It says he's not logged in, but from the URL he can see that there is something called the Vacation Checklist. He pastes the URL into incognito and when it opens, he realises that it's a public checklist.

Sid doesn't like that he had to select "Complete Task" after already having completed the subtasks (i.e. checkmark items). He thinks he should be able to simply select "Next" and the task will be marked as complete.

He doesn't like the fact that he'd have to create a new checklist every time he wanted to book a vacation.

Sid says the user experience of creating a new checklist from a template is not ideal. He would rather just run the checklist as is and rely on the tool to create a checklist for him automatically. 

He says there's not much he didn't like about the checklist. "It's pretty good."

**5. What did you like about the process?**

Sid likes that he can see which tasks he has completed, and which are still remaining. 

He also likes that the checklist has been split up into different sections.

<br>

**Other:**

**1. General notes**

After completing the checklist, Sid goes back to the unchecked tasks in the sidebar. He selects the "due" item under the checklist name expecting to see a due date displayed. Nothing happens.

After realising that the checklist is public, Sid says that it makes sense that pinging another user didn't work. He takes for granted that in the actual Workflow Manager it will. 

Sid hopes that there will be some kind of dashboard (on the homepage of the Workflow Manager) that displays his checklist, as well as the tasks he still needs to complete.

**2. Mentioned in user interview**

N/A
