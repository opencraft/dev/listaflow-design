# Remote Usability Tests - March 2021

## Introduction

Now that the [Discovery](https://gitlab.com/opencraft/dev/workflow-manager/-/blob/92a8aa6b9639cab64749c0f17977702460d8d7dc/docs/discoveries/workflow-manager-proposition-requirements-and-initial-steps.md) and [UX Discovery](https://gitlab.com/opencraft/dev/workflow-manager/-/blob/master/docs/discoveries/workflow-manager-ux-discovery.md) have been done, we all have an idea of what the Workflow Manager will be, and how it will improve processes at OpenCraft. Now’s where the real work begins!

We’ll be kicking off the project by building a new and improved version of the Vacation Checklist from the [OpenCraft handbook](https://handbook.opencraft.com/en/latest/vacation/#vacation-checklist). As such, for this remote portion of the usability test, we will be focussing on the Vacation Checklist in particular.

For this test, we have recreated the vacation checklist using Process Street. The goal is to get an idea of how user-friendly the checklist is, so that we know what to avoid, and what to emulate in the Workflow Manager.

**Note**: Rather than trying to cover everything in this test, we will use the video calls we have scheduled with each of you to discuss other topics, such as your experience working with tools like monday.com, your expectations for the Workflow Manager, and anything else you’d like to bring to our attention.

## Test Instructions

We’d like you to go through the steps of the Vacation Checklist, and record yourself and your screen whilst doing so.

#### A few things to know before you begin:


- Try to make the video about 15 minutes long (or longer).
- We want to understand your experience using the checklist, so please go slowly, and **SPEAK through your experience** so we can understand what you’re thinking/feeling through each step. 
- When you answer a question, read it out loud first so we know what question you’re answering. Try to elaborate on your thoughts to give us as much relevant information as possible.
- We would like you to record your screen, as well as your face when you do your video. You can use the **14 day free trial** on the [Loom Business Plan](https://www.loom.com/pricing) if you don’t already use a different service for this. (Please note: Loom’s "Starter" plan only allows recordings up to 5 minutes long, so please use the free trial on the Business Plan instead.) 
- The checklist we’re using for this test is specifically for developers, but it doesn’t matter if you’re not a developer! We just want to see the basic process you would follow through any checklist.
- You’ll notice that some of the steps in the checklist ask that you perform certain tasks like adding a calendar event, or posting on the forum. Although you don’t actually have to submit an event/post, please walk us through the steps you would take if you did.
- When your video is ready, please link to it inside the issue we’ve created for each of you in GitLab:
  - [Nizar](#9)
  - [Kahlil](#11)
  - [Fox](#12)
  - [Sid](#10)

Any questions? Please reach out to me (Ali).

**Let’s get started!**

---

## Complete the Vacation Checklist for Developers

#### Tasks
  1. Visit [this link](https://app.process.st/templates/Vacation-Checklist-for-Developers-qsA0QLMrsm-KEMZz6dpAEg/view/tasks/v77bemlHDOO6R0n3zXpMeQ) 
  1. Create your own instance of the Vacation Checklist
  1. Follow the steps in the checklist to manage your imaginary vacation application. Speak through each step as you’re busy with it.
  1. Once the checklist is complete, answer the questions below.

#### Questions
  1. Would you say the checklist was easy to use? Why, or why not?
  1. You may have noticed that some information from the OpenCraft handbook has been added to the checklist. What did you think about how the documentation and checklist have been combined? 
  1. Did you feel that the checklist items were in the correct order? If not, what would you change?
  1. What did you **dislike** about the process of following the Vacation Checklist?
  1. What did you **like** about the process?

---

## Thank you!
A big THANK YOU for taking part in the usability testing! Your insights will help to make the Workflow Manager the best it can be.

Please remember to add a link to your video in the description of your GitLab issue (see the link in the test instructions). 
