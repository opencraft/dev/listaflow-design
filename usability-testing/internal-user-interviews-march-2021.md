# User Interviews 

## Internal Test Users

The goal of the user interviews is to understand your hopes for the Workflow Manager. We’d like insight into how it could solve any problems you currently face when following workflows or processes, or the checklists associated with them. 

1. Can you tell me a bit about your role at OpenCraft? 
1. What problem/s do you hope the Workflow Manager will solve for you?
1. Which part of the Workflow Manager would you expect to use most often, or find the most useful? 
1. Would you find it useful if certain checklists were combined with details from the [OpenCraft handbook](https://handbook.opencraft.com/en/latest/)?
1. When communicating with other team members, do you rely more on email notifications, or in-app notifications?
1. How do you keep track of all the tasks that have been assigned to you? Are you satisfied with your current process? 
1. Do you often check the progress of a task or team? What type of progress display do you find most useful? 
1. Are there any tools you’ve used that you’d like to discuss? e.g. [monday.com](https://monday.com/), [Process Street](https://www.process.st/), [todoist](https://todoist.com/home), etc? Feel free to share your screen if you’d like to highlight specific aspects of the tool.
   1. Is there anything about the tool that you particularly like? If so, what?
   1. Do you dislike anything about the tool?
   1. Does the tool make it easy to set the status of a task? Why, or why not?
   1. What do you hope the Workflow manager will do better than the tool?
   1. Is there anything else about the tool that you’d like to mention?
1. Do you have any last thoughts you’d like to chat about before we wrap up?
