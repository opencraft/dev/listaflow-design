# User Interviews 

## External Test User

The goal for this interview is to determine whether a tool like the one we’re building at OpenCraft (called the [_Workflow Manager_](https://gitlab.com/opencraft/dev/workflow-manager/-/blob/92a8aa6b9639cab64749c0f17977702460d8d7dc/docs/discoveries/workflow-manager-proposition-requirements-and-initial-steps.md)) would be useful to other organisations. It would also be valuable to get insight into how workflows, processes, and checklists are managed at other companies, so that we can make sure that we focus on building the right features.

1. Can you tell me a bit about your current role at your organisation? 
1. I imagine there’s a fair amount of organisation that goes into managing the various people and processes at your company. How do you keep on top of this?
1. Does your team use checklists to manage their workflows? 
   1. If so, do you have any frustrations related to how checklists are currently being used at your organisation?
1. Do you use any tools for task management or issue tracking? If so:
   1. Is there anything about the tool that you particularly like? (feel free to share your screen if you’d like to highlight specific aspects of the tool)
   1. Do you dislike anything about the tool?
   1. What do you wish the tool would do better?
1. Is it important for you to be able to see the status of a particular task, or to monitor the progress of your team as a whole?
   1. If so, do you have a process in place for this?
   1. Do you feel the process works well for you and your team? Why, or why not?
1. By now, you may have an idea of what we have in mind for the tool we’d like to build. Do you think a tool like the _Workflow Manager_ would be useful to your organisation?
1. Do you have any last thoughts you’d like to chat about before we wrap up?
